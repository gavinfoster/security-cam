#!/home/gavin/.config/nvm/versions/node/v12.13.0/bin/node
const AWS = require('aws-sdk')
const fs = require('fs')
const util = require('util')
const config = require('./config.js')
const os = require('os')
const luxon = require('luxon')

const DateTime = luxon.DateTime
const unlink = util.promisify(fs.unlink)
const readdir = util.promisify(fs.readdir)
const readFile = util.promisify(fs.readFile)

const BUCKET_NAME = 'office-security-cam'

const main = async () => 
  try {
    AWS.config.update(config)
    const s3 = new AWS.S3()

    const fileNames = await readdir(`${os.homedir()}/Videos/security-cam/`)
    for (let newFile of fileNames) {
      const fileData = await readFile(`${os.homedir()}/Videos/security-cam/${newFile}`)
      const s3Params = {
        Bucket: BUCKET_NAME,
        Key: DateTime.local().toISO(),
        Body: fileData
      }
      const s3Data = await s3.putObject(s3Params).promise()
      unlink(`${os.homedir()}/Videos/security-cam/${newFile}`)
    }
  } catch (err) {
    console.log(err)
  }
}

main()
